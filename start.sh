#!/bin/bash
REPO='https://gitlab.com/plus-b-public/keys.git'

# set executing DIR
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
DIRPLUSB=${DIR}
cd $DIR

if [ -f ${DIR}/append_key_to_authorized_keys_file ]; then
  git remote set-url origin ${REPO}
  git pull
else
  DIRPLUSB=${DIR}/plusb_public_keys
  git clone ${REPO} ${DIRPLUSB}
fi

if ! (crontab -l | grep -q 'start.sh'); then
  MINUTEN=$(awk -v min=0 -v max=59 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
  (crontab -l; echo "${MINUTEN} * * * * ${DIRPLUSB}/start.sh") | crontab -
fi

cd ${DIRPLUSB}
. ${DIRPLUSB}/append_key_to_authorized_keys_file
