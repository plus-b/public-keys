# plus B public-keys

This repository contains public SSH keys of plus B GmbH employees.

## Prerequisites

**IMPORTANT**: The last Line must be empty in pub-files.

## Install

```
curl -s https://gitlab.com/plus-b-public/keys/-/raw/master/start.sh | bash -s
```

with wget

```
tmpstring=$(date +%s).sh && wget -O $tmpstring https://gitlab.com/plus-b-public/keys/-/raw/master/start.sh && chmod +x $tmpstring && ll && rm $tmpstring -f
```

## Test

```
cat ~/.ssh/authorized_keys
```
